package com.yhh.data.entity;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class Book implements Serializable {
    private static final long serialVersionUID = 7251922943477570209L;
    private int id;
    private String name;
    private Date createTime;
    private Date updateTime;
}
