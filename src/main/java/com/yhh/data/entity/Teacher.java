package com.yhh.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Setter
@Getter
public class Teacher {
    @Id
    private int id;
    private String name;
    private String title;
}