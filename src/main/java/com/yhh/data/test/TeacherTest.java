package com.yhh.data.test;

import com.yhh.data.entity.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class TeacherTest {
    /**
     * @param args
     */
    public static void main(String[] args) {
        Teacher t = new Teacher();
        t.setId(1);
        t.setName("t1");
        t.setTitle("12");

        Configuration cfg = new Configuration();
        cfg.configure();//读取配置文件

        ServiceRegistry serviceRegistry =new ServiceRegistryBuilder().
                applySettings(cfg.getProperties()).buildServiceRegistry();

        SessionFactory factory = cfg.configure().buildSessionFactory(serviceRegistry);

        Session session = factory.openSession();
        session.beginTransaction();
        session.save(t);
        session.getTransaction().commit();
        session.close();
        factory.close();
    }
}
