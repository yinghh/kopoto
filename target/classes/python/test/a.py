# coding:utf-8

import html.parser as h
import sys
from urllib.request import urlopen


#
class MyHTMLParser(h.HTMLParser):
    a_t = False
    count = 0
    str = ""

    def handle_starttag(self, tag, attrs):
        if self.count > 0:
            self.count += 1
            if str(tag).startswith("script"):
                self.a_t = True
            else:
                self.a_t = False
        if str(tag).startswith("div"):
            for attr in attrs:
                if attr[0] == "id" and attr[1] == "content":
                    self.count += 1

    def handle_endtag(self, tag):
        if self.count > 0:
            self.count -= 1

    def handle_data(self, data):
        if self.count > 0:
            if self.a_t is False:
                self.str += data


def getHtml(url):
    page = urlopen(url)
    html = page.read()
    try:
        resultStr = html.decode("gbk")
    except:
        try:
            resultStr = html.decode("utf-8")
        except:
            resultStr = html.decode("gb2312")
    return resultStr


def getdatabybookidandchapernum(baseurl, bookid, chapternum):
    try:
        real_url = baseurl + "/" + str(bookid) + "/" + chapternum
        html = getHtml(real_url)
        p = MyHTMLParser()
        p.feed(html)
        p.close()
        result = {"content": p.str}
        result["success"] = 1
    except:
        result = {"success": 0}
    return result


def main(baseurl, bookid, chapternum):
    return getdatabybookidandchapernum(baseurl, bookid, chapternum)


if __name__ == '__main__':
    base_url = "http://www.biquge.la/book"
    book_id = 8
    chapter_num = "5752.html"
    print(main(sys.argv[1], sys.argv[2], sys.argv[3]))
    # print(main(base_url, book_id, chapter_num))
