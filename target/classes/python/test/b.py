import os
from urllib.request import urlopen, urlretrieve
import html.parser as h

import sys


class ContentHTMLParser(h.HTMLParser):
    flag = False
    url = "http://www.biquge.la/book/"
    count = 1
    href = ""
    bookId = 1
    realUrls = []

    def handle_starttag(self, tag, attrs):
        if str(tag) == "dd":
            self.flag = True
        if self.flag is True:
            if str(tag) == "a":
                for attr in attrs:
                    if attr[0] == "href":
                        self.href = attr[1]

    def handle_endtag(self, tag):
        if str(tag) == "dd":
            self.flag = False
        if self.flag is True:
            if str(tag) == "a":
                self.href = ""

    def handle_data(self, data):
        if self.flag is True:
            if self.href != "":
                try:
                    self.realUrls.append(self.href)
                finally:
                    self.count += 1


# 下载HTML
def getHtml(url):
    page = urlopen(url)
    html = page.read()
    try:
        resultStr = html.decode("gbk")
    except:
        try:
            resultStr = html.decode("uft-8")
        except:
            resultStr = html.decode("gb2312")
    return resultStr


def main(baseUrl, bookId):
    url = baseUrl+"/" + bookId + "/"
    html = getHtml(url)
    p = ContentHTMLParser()
    p.bookId = bookId
    p.feed(html)
    p.close()
    result = {"content": p.realUrls}
    result["success"] = 1
    result["bookId"] = bookId
    result["baseUrl"] = baseUrl
    print(result)


if __name__ == '__main__':
    print(main(sys.argv[1], sys.argv[2]))
